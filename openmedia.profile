<?php


/**
 * @file
 * Installation profile that configures a site as a central hub for managing
 * a local community's public access channel.
 *
 * Some of the constants at the top of this file can be changed to
 * customize the profile for your site.
 */

//----------------------------------------
// Settings you probably want to customize
//----------------------------------------


//----------------------------------------
// Settings you might want to customize
//----------------------------------------


//----------------------------------------
// Settings you should not change
//----------------------------------------
define('OPENMEDIA_ROLE_ANONYMOUS', 1);
define('OPENMEDIA_ROLE_AUTHENTICATED', 2);
define('OPENMEDIA_ROLE_ADMINISTRATOR', 3);

//----------------------------------------
// Profile code
//----------------------------------------

function openmedia_profile_modules() {
  return array(
    // CORE MODULES.
    // Required.
    'block',
    'filter',
    'node',
    'system',
    'user',

    // Optional as per http://drupal.org/node/27367
    'taxonomy',  // Load this first, otherwise stuff might break.
    'book',
    'comment',
    'dblog',
    'help',
    'menu',
    'path',
    'profile',
    'search',
    'update',
    'upload',

    // CONTRIB MODULES
    'install_profile_api',

    // CCK and views.
    'content',
    'views',
    'views_ui',

    // CCK-related modules.
    'content_copy',
    'fieldgroup',
    'filefield',
    'imagefield',
    'number',
    'nodereference',
    'optionwidgets',
    'text',

    // Date modules.
    'date_api',  // This must be loaded before any other data modules.
    'date',
    'date_popup',
    'date_timezone',

    // MERCI must be enabled after the date module!
    'merci',

    // Other modules.
    'creativecommons',
    'colorpicker',
    'imageapi',
    'imagecache',
    'location',
    'og',
    'om_project',
    'om_support',
    'pathauto',
    'stringoverrides',
    'token',
  );
}

function openmedia_profile_details() {
  return array(
    'name' => 'Open Media',
    'description' => "Install profile to configure a site as a central hub for managing a local community's public access channel",
  );
}

/**
 * Implementation of hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
 */
function openmedia_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {

  }
}

function openmedia_profile_task_list() {
  return array(
    'openmedia-tutorial' => t('View tutorial'),
    'openmedia-start-batch' => t('Open Media installation'),
  );
}

function openmedia_profile_tasks(&$task, $url) {

  switch ($task) {
    case 'profile':
      variable_set('install_task', 'openmedia-tutorial');
      drupal_goto($url);
      break;
    case 'openmedia-tutorial':
      $output = '';
      // TODO: This is a dummy youtube video for now, replace it with the
      // tutorial.
      $output .= '<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/cevs63S5XLo&hl=en&fs=1&rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/cevs63S5XLo&hl=en&fs=1&rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></embed></object>';
      $output .= drupal_get_form('openmedia_begin_installation_form', $url);
      return $output;
      break;
    case 'openmedia-start-batch':
      // If the files directory isn't writable, then exit.
      if (_openmedia_configure_files()) {
        // Start a batch, switch to 'openmedia-batch' task. We need to
        // set the variable here, because batch_process() redirects.
        variable_set('install_task', 'openmedia-batch');
        _openmedia_set_batch($task, $url);
      }
      // Files directory creation failed, skip the rest of the setup.
      else {
        $task = 'profile-finished';
      }
      break;
    case 'openmedia-batch':
      // We are running a batch install of the profile.
      // This might run in multiple HTTP requests, constantly redirecting
      // to the same address, until the batch finished callback is invoked
      // and the task advances to 'profile-finished'.
      include_once 'includes/batch.inc';
      $output = _batch_page();
      return $output;
      break;
  }
}

/**
* Sets up the batch processing of the install profile tasks.
*/
function _openmedia_set_batch(&$task, $url) {
  $batch = array(
    'operations' => array(
      array('_openmedia_batch_dispatch', array('_openmedia_create_node_types', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_configure_site', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_configure_theme', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_configure_comment', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_configure_attachments', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_create_roles', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_create_menus', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_configure_blocks', array())),
      array('_openmedia_batch_dispatch', array('_openmedia_rebuild_menu', array())),
    ),
    'title' => t('Setting up the Open Media site...'),
    'finished' => '_openmedia_batch_finished',
  );
  batch_set($batch);
  batch_process($url, $url);
}

/**
 * Dispatch function for the batch processing. This allows us to do some
 * consistent setup across page loads while breaking up the tasks.
 *
 * @param $function
 *   The function to dispatch to.
 * @param $args
 *   Any args passed to the function from the batch.
 * @param $context
 *   The batch context.
 */
function _openmedia_batch_dispatch($function, $args, &$context) {
  // If not in 'safe mode', increase the maximum execution time:
  if (!ini_get('safe_mode')) {
    set_time_limit(0);
  }
  install_include(openmedia_profile_modules());
  $function($args, $context);
}

/**
* Batch 'finished' callback
*/
function _openmedia_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = count($results) .' actions processed.';
    $message .= theme('item_list', $results);
    $type = 'status';
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = 'An error occurred while processing '. $error_operation[0] .' with arguments :'. print_r($error_operation[0], TRUE);
    $type = 'error';
  }

  // Clear out any status messages that modules may have thrown, as we're
  // setting our own for the profile.  Leave error messages, however.
  drupal_get_messages('status');
  drupal_set_message($message, $type);

  // Advance the installer task.
  variable_set('install_task', 'profile-finished');
}

function openmedia_begin_installation_form(&$form_state, $url) {
  $form = array();
  $form['intro'] = array(
    '#value' => '<p>' . st("Please watch the informational video above, then click 'Begin installation' when ready.") . '</p>',
  );

  $form['submit'] = array(
  '#type' => 'submit',
  '#value' => st('Begin installation'),
  );
  $form['#action'] = $url;

  return $form;
}

function openmedia_begin_installation_form_submit($form, &$form_state) {
  // Trigger the start of the installation batch.
  variable_set('install_task', 'openmedia-start-batch');
  $form_state['redirect'] = $form['#action'];
}



function _openmedia_create_node_types($args, &$context) {
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st('If you want to add a static page, like a contact page or an about page, use a page.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'story',
      'name' => st('Story'),
      'module' => 'node',
      'description' => st('Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
  );
  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
    // Store some result for post-processing in the finished callback.
    $context['results'][] = t('Set up node type %type.', array('%type' => $type->name));
  }

  // Default page to not be promoted.
  // variable_set('node_options_page', array('status'));
  $context['message'] = t('Set up basic node types');
}

function _openmedia_configure_site($args, &$context) {
  variable_set('cache', CACHE_NORMAL);
  $context['results'][] = t('Configured site settings.');
  $context['message'] = t('Configured site settings');
}

function _openmedia_configure_theme($args, &$context) {
  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);
  // Set TVframe to the default theme.
  install_default_theme('TVframe');
  install_admin_theme('garland');
  $context['results'][] = t('Configured default theme.');
  $context['message'] = t('Configured default theme');
}

function _openmedia_configure_comment($args, &$context) {

  $types = array(
    'story',
  );

  foreach ($types as $type) {
    variable_set('comment_' . $type, COMMENT_NODE_READ_WRITE);
    variable_set('comment_preview_' . $type, COMMENT_PREVIEW_OPTIONAL);
    variable_set('comment_default_order_' . $type, COMMENT_ORDER_OLDEST_FIRST);
  }

  $types = array(
    'page',
  );
  foreach ($types as $type) {
    variable_set('comment_' . $type, COMMENT_NODE_DISABLED);
  }

  $context['results'][] = t('Configured comment settings.');
  $context['message'] = t('Configured comment settings');
}

function _openmedia_configure_attachments($args, &$context) {

  // These node types have attachments enabled.
  $types = array(
  );
  foreach ($types as $type) {
    variable_set('upload_' . $type, 1);
  }

  // These node types have attachments disabled.
  $types = array(
  );
  foreach ($types as $type) {
    variable_set('upload_' . $type, 0);
  }

  // Set default upload extensions.
  // variable_set('upload_extensions_default', 'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp tar gz tgz');

  $context['results'][] = t('Configured attachment settings.');
  $context['message'] = t('Configured attachment settings');
}

/**
 * Setup basic roles and permissions.
 */
function _openmedia_create_roles($args, &$context) {
  // Map role names to role ID constants.
  $roles = array(
    OPENMEDIA_ROLE_ANONYMOUS => 'anonymous',
    OPENMEDIA_ROLE_AUTHENTICATED => 'authenticated',
    OPENMEDIA_ROLE_ADMINISTRATOR => 'administrator',
  );

  // Define permissions for each role ID.
  $permissions = array(
    OPENMEDIA_ROLE_ANONYMOUS => array(
      // comment
      'access comments',
      // node
      'access content',
      // search
      'search content',
      'use advanced search',
      // user
      'access user profiles',
    ),
    OPENMEDIA_ROLE_AUTHENTICATED => array(
      // comment
      'access comments',
      'post comments',
      'post comments without approval',
      // node
      'access content',
      // search
      'search content',
      'use advanced search',
      // user
      'access user profiles',
      'change own username',
    ),
    OPENMEDIA_ROLE_ADMINISTRATOR => array(
      // block module
      'administer blocks',
      // comment module
      'access comments',
      'administer comments',
      'post comments',
      'post comments without approval',
      // filter module
      'administer filters',
      // menu module
      'administer menu',
      // node module
      'access content',
      'administer content types',
      'administer nodes',
      'create page content',
      'create story content',
      'delete any page content',
      'delete any story content',
      'delete own page content',
      'delete own story content',
      'edit any page content',
      'edit any story content',
      'edit own page content',
      'edit own story content',
      'revert revisions',
      'view revisions',
      // path module
      'administer url aliases',
      'create url aliases',
      // search module
      'search content',
      'use advanced search',
      'administer search',
      // system module
      'access administration pages',
      'access site reports',
      'administer actions',
      'administer files',
      'administer site configuration',
      // user module
      'access user profiles',
      'administer permissions',
      'administer users',
      'change own username',
    ),
  );

  // Delete current roles and permissions and re-populate them.
  db_query('TRUNCATE {role}');
  db_query('TRUNCATE {permission}');

  foreach ($roles as $rid => $name) {
    db_query("INSERT INTO {role} (rid, name) VALUES (%d, '%s')", $rid, $name);
  }
  $context['results'][] = t('Created roles.');
  foreach ($permissions as $rid => $perms) {
    db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (%d, '%s', 0)", $rid, implode(', ', $perms));
  }
  $context['results'][] = t('Set role permissions.');
  $context['message'] = t('Created roles and set permissions');
}

/**
 * Setup menus.
 */
function _openmedia_create_menus($args, &$context) {
  $items = array();

  // Finally, save all these customizations.
  foreach ($items as $item) {
    menu_link_save($item);
    $context['results'][] = t('Created menu item %name at %path.', array('%name' => $item['link_title'], '%path' => $item['link_path']));
  }

  $context['message'] = t('Created menus');
}

function _openmedia_configure_blocks($args, &$context) {

  // Clear out existing blocks.
  db_query("DELETE FROM {blocks}");

  // User blocks.
  install_add_block('user', 0, 'garland', 1, -4, 'right');
  install_add_block('user', 1, 'garland', 1, -2, 'right');

  // Drupal footer.
  install_add_block('system', 0, 'garland', 1, 10, 'footer');

  _block_rehash();

  $context['results'][] = t('Configured blocks.');
  $context['message'] = t('Configured blocks');
}

function _openmedia_rebuild_menu($args, &$context) {
  menu_rebuild();
  $context['results'][] = t('Rebuilt menus.');
  $context['message'] = t('Rebuilt menus');
}

/**
 * Make sure the core file system is set up properly
 * and that the files directory is writable by the web
 * server.
 *
 * @return
 *   If FALSE, then the files directory is not properly
 *   set up or is not writable by the web server.
 */
function _openmedia_configure_files() {
  $directory = file_directory_path();
  if (!file_check_directory($directory, TRUE)) {
    // Permissions are not properly set to allow
    // server to create files.  Therefore, present an
    // error message.
    drupal_set_message(t('The %files directory was either not able to be created or is not writable by the web server.  In order for the !profile_name profile to install properly, the web server must be able to create files and directories in the Drupal files directory.  Please adjust the permissions of your file system so that the web server has the appropriate access to the %files directory and then reinstall the !profile_name profile.', array('%files' => $directory, '!profile_name' => $profile_name)), 'error');
    return FALSE;
  }

  // Set these now so we're extra sure our file creation behaves consistently.
  variable_set('file_directory_path', $directory);
  variable_set('file_directory_temp', file_directory_temp());
  variable_set('file_downloads', FILE_DOWNLOADS_PUBLIC);

  return TRUE;
}

